package io;

public enum BPMNLabel {
	
	STARTEVENT,
	ENDEVENT,
	TASK,
	MESSAGE,
	XOR,
	POLYGON,
	LOOP,
	AND;
}
