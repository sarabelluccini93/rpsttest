package algo;


import spec.ISpec;

public interface ITranslationAlg {
	
	/*
	 * It gives a specification as result
	 */
	public ISpec getSpec();
}
